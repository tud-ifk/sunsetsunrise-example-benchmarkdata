# Changelog

<!--next-version-placeholder-->

## v0.2.0 (2023-02-22)
### Feature
* Add Venn diagram and finalize notebook ([`5fa647b`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/sunsetsunrise-example-benchmarkdata/-/commit/5fa647b64b8ae002f2760a48f80b92b7c307b91f))

### Documentation
* Add cover figure ([`3a8139a`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/sunsetsunrise-example-benchmarkdata/-/commit/3a8139abf376e174f4e685b2d54d501f0f7fee54))
* Update HTML to latest ([`3109460`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/sunsetsunrise-example-benchmarkdata/-/commit/3109460de37eb04f42a0b327f1d6d077a6483902))
* Add two sample maps ([`8580402`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/sunsetsunrise-example-benchmarkdata/-/commit/8580402de2039f1fc8bfcc77159deeb4cbb91f10))
