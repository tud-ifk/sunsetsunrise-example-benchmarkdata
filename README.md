[![version](https://kartographie.geo.tu-dresden.de/ad/sunsetsunrise-demo/version.svg)][static-gl-url] [![pipeline](https://kartographie.geo.tu-dresden.de/ad/sunsetsunrise-demo/pipeline.svg)][static-gl-url]


# From sunrise to sunset - Demo for benchmark data use

![Example map](resources/hll_intersection_deusca.png?raw=true)

In the paper by Dunkel et al. (2023a), we shared benchmark data as Supporting Information in
a repository (Dunkel et al 2023b). The data was abstracted using HyperLogLog and can be (limitedly)
used beyond the original application described in the article.

> Dunkel, A., Hartmann, M. C., Hauthal, E., Burghardt, Dirk, & Purves, R. S. (2023a).
  From sunrise to sunset: Exploring landscape preference through global reactions to
  ephemeral events captured in georeferenced social media. PLoS ONE, 17(1). [DOI](https://doi.org/10.1371/journal.pone.0280423)

> Dunkel, A., Hartmann, M. C., Hauthal, E., Burghardt, Dirk, & Purves, R. S. (2023b).
  Supplementary materials for the publication “From sunrise to sunset:
  Exploring landscape preference through global reactions to ephemeral
  events captured in georeferenced social media” \[Data set\]. OpARA. [DOI](https://doi.org/10.25532/OPARA-200)

This repository contains a short Jupyter notebook that illustrates how the shared HyperLogLog data can be loaded and used.
- see the folder [notebooks/](notebooks/) for the `ipynb` notebook
- the notebook converted to HTML can be found [here][1].

## Developers

This repository is versioned with [python-semantic-release](https://python-semantic-release.readthedocs.io/en/latest/),
Jupyter notebooks are tracked as Markdown files using [Jupytext](https://github.com/mwouts/jupytext). If you want
to run these notebooks, have a look at the instructions to use the [Carto-Lab Docker](https://gitlab.vgiscience.de/lbsn/tools/jupyterlab), 
provided at the beginning of the [notebook][1].

To bump a version:
```python
semantic-release publish
```

[1]: https://kartographie.geo.tu-dresden.de/ad/sunsetsunrise-demo/html/intro.html
[static-gl-url]: https://gitlab.hrz.tu-chemnitz.de/tud-ifk/ad/sunsetsunrise-example-benchmarkdata
